package com.shoppinglist.resource;

import static org.junit.Assert.*;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.http.HttpStatus;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.shoppinglist.model.ListItem;

@RunWith(Arquillian.class)
public class ShoppingListResourceIntTest {

	@ArquillianResource
	private URL url;

	private Client client = ClientBuilder.newClient();

	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.shoppinglist")
				.addPackages(true, "org.apache.poi", "org.openxmlformats.schemas")
				.addPackages(true, "org.apache.commons", "commons-lang3")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", "org.mockito:mockito-core:1.9.5")
								.withTransitivity().asFile());
	}
	
	// Create
//	@Test
//	@RunAsClient
//	public void addItem(@ArquillianResource final WebTarget webTarget) throws Exception {
//		final Response response = webTarget
//				.path("/add")
//				.request(MediaType.APPLICATION_JSON)
//				.put(Entity.json(new ListItem("New Item", 5, true)));
//		assertEquals(201, response.getStatus());
//	}
	
	// Read
	@Test
	@RunAsClient
	public void getShoppingList() throws Exception {
		URI uri = UriBuilder.fromUri("http://localhost:8080/shoppinglist/api/shoppinglist").build();
		WebTarget webTarget = client.target(uri);
		Response response = webTarget.request().get();
		assertEquals(200, response.getStatus());
	}
	
	// Update
//	@Test
//	@RunAsClient
//	public void updateItem(@ArquillianResource final WebTarget webTarget) throws Exception {
//		final Response response = webTarget
//				.path("/update")
//				.request(MediaType.APPLICATION_JSON)
//				.put(Entity.json(new ListItem("Eggs, Dozen", 2, true)));
//		assertEquals(201, response.getStatus());
//	}
	
	// Delete
	@Test
	@RunAsClient
	public void deleteItem() throws Exception {
		URI uri = UriBuilder.fromUri("http://localhost:8080/shoppinglist/api/shoppinglist/3").build();
		WebTarget webTarget = client.target(uri);
		Response response = webTarget.request().delete();
		assertEquals(200, response.getStatus());
	}
}
