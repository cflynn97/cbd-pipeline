package com.shoppinglist.resource;

import java.util.List;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.shoppinglist.common.error.ErrorMessage;
import com.shoppinglist.common.error.ErrorMessages;
import com.shoppinglist.exception.ItemNotFoundException;
import com.shoppinglist.exception.ItemAlreadyAddedException;
import com.shoppinglist.model.ListItem;
import com.shoppinglist.services.ShoppingListServices;

@Path("/shoppinglist")
public class ShoppingListResource {
	
	@Inject
	ShoppingListServices shoppingListServices;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	public Response findAll() {
		final List<ListItem> shoppingList = shoppingListServices.findAll();
		return Response.status(200).entity(shoppingList).build();
	}
	
	@PUT 
	@Path("/add")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response add(final ListItem listItem) {
		Response response;
		try {
			shoppingListServices.add(listItem);
			response = Response.status(201).entity(listItem).build();
		} catch (ItemAlreadyAddedException iaae) {
			response = Response.status(403).entity(new ErrorMessage(ErrorMessages.DUPLICATE_ENTRY.getMsg())).build();
		}
		return response;
	}
	
	@PUT 
	@Path("/update")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response update(ListItem listItem) {
		Response response;
		try {
			shoppingListServices.update(listItem);
			response = Response.status(201).entity(listItem).build();
		} catch (EJBException e) {
			response = Response.status(403).entity(new ErrorMessage(ErrorMessages.INVALID_FIELD.getMsg())).build();
		}
		return response;
	}
	
	@DELETE 
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response delete(@PathParam("id") final Long id) {
		Response response;
		try {
			shoppingListServices.delete(id);
			response = Response.status(200).entity("List Item " + id + " deleted").build();
		} catch (EJBException e) {
			response = Response.status(404).entity(new ErrorMessage(ErrorMessages.LIST_ITEM_NOT_FOUND.getMsg())).build();
		}
		return response;
	}
}