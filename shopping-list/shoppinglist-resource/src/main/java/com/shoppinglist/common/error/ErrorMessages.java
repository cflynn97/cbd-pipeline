package com.shoppinglist.common.error;

public enum ErrorMessages {
	INVALID_FIELD("Invalid field"),
	DUPLICATE_ENTRY("Item already on the shopping list"),
	LIST_ITEM_NOT_FOUND("Item not found");
	
	private String errorMessage;
	
	ErrorMessages(String errMsg){
		this.errorMessage=errMsg;
	}
	
	public String getMsg(){
		return errorMessage;
	}
}
