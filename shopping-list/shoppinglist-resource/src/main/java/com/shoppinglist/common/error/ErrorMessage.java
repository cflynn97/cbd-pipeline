package com.shoppinglist.common.error;

public class ErrorMessage {
	String msg;
	
	public ErrorMessage(String message) {
		this.msg=message;
	}
	
	public String getErrorMessage() {
		return msg;
	}
}
