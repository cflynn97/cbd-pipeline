package com.shoppinglist.resource;

import static com.shoppinglist.commontests.listitem.ListItemForTestsRepository.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ejb.EJBException;
import javax.ws.rs.core.Response;

import com.shoppinglist.common.error.ErrorMessage;
import com.shoppinglist.exception.ItemAlreadyAddedException;
import com.shoppinglist.exception.ItemNotFoundException;
import com.shoppinglist.model.ListItem;
import com.shoppinglist.services.ShoppingListServices;

class ShoppingListResourceUTest {
	
	private ShoppingListResource shoppingListResource;
	
	@Mock
	private ShoppingListServices shoppingListServices;
	
	@BeforeEach
	public void initTestCase() {
		MockitoAnnotations.initMocks(this); 
		shoppingListResource = new ShoppingListResource();
		shoppingListResource.shoppingListServices = shoppingListServices;
	}
	
	// Create
	@Test
	void createValid() {
		final Response response = shoppingListResource.add(listItemWithId(bread(), 1L));
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_CREATED)));
		assertThat(response.getEntity().toString(), is(equalTo("ListItem [id=1, itemName=Half Slice Pan, quantity=2, purchased=false]")));
	}
	
	@Test
	void createInvalidAlreadyExists() {
		when(shoppingListServices.add(bread())).thenThrow(new ItemAlreadyAddedException());
		final Response response = shoppingListResource.add(bread());
		assertEquals(HttpStatus.SC_FORBIDDEN, response.getStatus());
		ErrorMessage message = (ErrorMessage) response.getEntity();
		assertEquals("Item already on the shopping list", message.getErrorMessage());
	}

	// Read
	@Test
	void findShoppingListEmpty() {
		when(shoppingListServices.findAll()).thenReturn(new ArrayList<>());

		final Response response = shoppingListResource.findAll();
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_OK)));
		@SuppressWarnings("unchecked")
		List<ListItem> shoppingList = (List<ListItem>) response.getEntity();
		assertEquals(0, shoppingList.size());	
	}

	@Test
	void findShoppingListTwoItems() {
		when(shoppingListServices.findAll()).thenReturn(
				Arrays.asList(listItemWithId(bread(), 1L), listItemWithId(milk(), 2L)));

		final Response response = shoppingListResource.findAll();
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_OK)));
		@SuppressWarnings("unchecked")
		List<ListItem> shoppingList = (List<ListItem>) response.getEntity();
		assertEquals(2, shoppingList.size());	
	}
	
	// Update
	@Test
	void updateValid() {
		final Response response = shoppingListResource.update(listItemWithId(bread(), 1L));
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_CREATED)));
		assertThat(response.getEntity().toString(), is(equalTo("ListItem [id=1, itemName=Half Slice Pan, quantity=2, purchased=false]")));
	}
	
	@Test
	void updateInvalidNull() {
		ListItem listItem = new ListItem();
		doThrow(new EJBException()).when(shoppingListServices).update(listItemWithId(listItem, 1L));
		final Response response = shoppingListResource.update(listItem);
		assertEquals(HttpStatus.SC_FORBIDDEN, response.getStatus());
		ErrorMessage message = (ErrorMessage) response.getEntity();
		assertEquals("Invalid field", message.getErrorMessage());
	}
	
	// Delete
	@Test
	void delete() {
		final Response response = shoppingListResource.delete(1L);
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_OK)));
	}
	
	// Delete
	@Test
	void deleteInvalid() {
		doThrow(new EJBException()).when(shoppingListServices).delete(4L);
		final Response response = shoppingListResource.delete(4L);
		assertThat(response.getStatus(), is(equalTo(HttpStatus.SC_NOT_FOUND)));
		ErrorMessage message = (ErrorMessage) response.getEntity();
		assertEquals("Item not found", message.getErrorMessage());
	}
}