var apiUrl = "http://localhost:8080/shoppinglist/api/shoppinglist";

$(document).ready(function() {
   getShoppingList();

   $("#editList").click(function(event) {
      $(this).hide();
      $(".list-functions").append('<a id="addItem">Add to List</a><a id="cancelEdit">Cancel</a>')
      $("#shoppingList").append('<tr><td class="purchasedCell"></td><td class="la"><input id="inputItemName" placeholder="Item name..." type="text"/></td><td><input id="inputQuantity" placeholder="#" type="text"/></td><td></td></tr>');
   });

   // Add Item
   $(document).on('click', '#addItem', function(event) {
      if(!$("#inputItemName").val() || !$("#inputQuantity").val()) {
         return;
      }

      let listItem = JSON.stringify({
         'itemName': $("#inputItemName").val(),
         'quantity': parseInt($("#inputQuantity").val()),
         'purchased':false
      });

      console.log(listItem);
      addToShoppingList(listItem);
   });

   // Remove Item
   $(document).on('click', 'i.fa-times-circle', function(event) {
      deleteListItem($(this).attr('value'));
   });

   // Change Item status
   $(document).on('click', 'i.fa-check-circle', function(event) {
      let purchasedState = $(this).parent().parent().children('td:first-of-type').hasClass('purchased');

      var listItem = JSON.stringify({
         'id': $(this).parent().parent().children('td:last-of-type').children('i').attr('value'),
         'itemName': $(this).parent().parent().children('td:nth-of-type(2)').html(),
         'quantity': parseInt($(this).parent().parent().children('td:nth-of-type(3)').html()),
         'purchased': !purchasedState
      });

      updateListItem(listItem);
   });

   // Cancel Add item
   $(document).on('click', '#cancelEdit', function(event) {
      $("#editList").show(); $("#addItem, #cancelEdit").remove();
      $("#shoppingList tr:last-of-type").remove();
   });
});

// CREATE queries
var addToShoppingList = function(listItem) {
   $.ajax({
      type: 'PUT',
      contentType: 'application/json',
      url: apiUrl + "/add",
      dataType: "json",
      data: listItem,
      success: function(data, textStatus, jqXHR) {
         $("#editList").show(); $("#addItem, #cancelEdit").remove();
         getShoppingList();
      }, error: function(data, textStatus, jqXHR) {
         alert(data +": "+textStatus + ": " + jqXHR);
      }
   });
}

// READ queries
var getShoppingList = function() {
   console.log('getShoppingList');
   $.ajax({
      type: 'GET',
      url: apiUrl,
      dataType: "json",
      success: function(data, textStatus, jqXHR) {
         $("#shoppingList").html('<tr><th>Done</th><th class="la">Item Name</th><th>Quantity</th><th>Remove</th></tr>');
         $.each(data, function(index, item) {
            $("#shoppingList").append('<tr><td class="purchasedCell item'+index+'"><i class="far fa-check-circle"></i></td><td class="la">' + item.itemName + '</td><td>' + item.quantity + '</td><td><i value="' + item.id + '" class="far fa-times-circle"></i></td></tr>');
            if(item.purchased > 0) { $(".item"+index).addClass('purchased').html('<i class="fas fa-check-circle"></i>').unbind(); }
         });
      }, error: function(data, textStatus, jqXHR) {
         alert(data +": "+textStatus + ": " + jqXHR);
      }
   });
};

// UPDATE queries
var updateListItem = function(listItem) {
   console.log('updateSneaker');
   $.ajax({
      type: 'PUT',
      contentType: 'application/json',
      url: apiUrl + "/update",
      data: listItem,
      success: function(data, textStatus, jqXHR) {
         getShoppingList();
      }, error: function(data, textStatus, jqXHR) {
         alert(data +": "+textStatus + ": " + jqXHR);
      }
   });
}

// DELETE queries
var deleteListItem = function(itemName) {
   console.log('unfulfilOrders');
   $.ajax({
      type: 'DELETE',
      url: apiUrl + "/" + itemName,
      success: function(data, textStatus, jqXHR) {
         getShoppingList();
      }, error: function(data, textStatus, jqXHR) {
         alert(data +": "+textStatus + ": " + jqXHR);
      }
   });
}
