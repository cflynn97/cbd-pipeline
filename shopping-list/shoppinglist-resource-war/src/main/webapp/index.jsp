<html>
	<head>
	  	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	  	<link rel="shortcut icon" type="image/svg" href=""/>
	  	<title>Shopping List</title>
	  	<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/index.css"/>
		<link rel="stylesheet" href="css/all.css" />

		<!-- Fonts -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato">

		<!-- JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script type="text/javascript">window.FontAwesomeConfig = { autoReplaceSvg: false }</script>
		<script defer src="js/all.js"></script>
		<script defer src="js/index.js"></script>
	</head>
	<body>
		<div class="main fw flex-cc">
			<div>
				<h1><i id="editList" class="fas fa-plus"></i>Grocery List</h1>
				<table id="shoppingList"></table>
				<div class="list-functions"></div>
			</div>
		</div>
	</body>
</html>
