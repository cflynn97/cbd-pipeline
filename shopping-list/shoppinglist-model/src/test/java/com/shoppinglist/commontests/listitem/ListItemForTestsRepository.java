package com.shoppinglist.commontests.listitem;

import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;

import com.shoppinglist.model.ListItem;

@Ignore
public class ListItemForTestsRepository {

	public static ListItem bread() {
		return new ListItem("Half Slice Pan", 2, false);
	}

	public static ListItem milk() {
		return new ListItem("Full Fat, 2L", 2, false);
	}
	
	public static ListItem eggs() {
		return new ListItem("Eggs, Dozen", 1, false);
	}
	
	public static ListItem listItemWithId(ListItem listItem, Long id) {
		listItem.setId(id);
		return listItem;
	}

	public static List<ListItem> shoppingList() {
		return Arrays.asList(bread(), milk(), eggs());
	}

}