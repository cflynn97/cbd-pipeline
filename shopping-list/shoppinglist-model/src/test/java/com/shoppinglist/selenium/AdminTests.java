package com.shoppinglist.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

class AdminTests {

	WebDriver driver;
	ChromeOptions options;
	
	@BeforeEach
	public void setUp() { 
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Christopher\\Documents\\MASE Projects\\Semester 2\\Group Project\\chromedriver.exe");
		options = new ChromeOptions();
		options.addArguments("--headless");
		options.addArguments("--no-sandbox");
		driver = new ChromeDriver();
		Dimension d = new Dimension(1920,1080);
		driver.manage().window().setSize(d);
		driver.get("http://localhost:8080/sneakerstore/");
		
		driver.findElement(By.id("action")).click();
		driver.findElement(By.id("userName")).sendKeys("admin");
		driver.findElement(By.id("password")).sendKeys("root");
		driver.findElement(By.id("login")).click();
	}
	
	//@Test
	public void testCustomerOrderAndCancel() throws Throwable {
		/* CHECK SNEAKER INVENTORY */
		Thread.sleep(250);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div[1]/a[2]")).click();
		Thread.sleep(250);
		assertEquals(driver.findElements(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div[2]/div/div[3]/div[2]/table/tbody/tr")).size(), 5);
		
		/* CHECK TWO ORDERS */
		Thread.sleep(250);
		driver.findElement(By.id("orders")).click();
		Thread.sleep(250);
		assertEquals(driver.findElements(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div[2]/div/table/tbody/tr")).size(), 2);
		
		/* CHECK ORDER FULFILLMENT */
		Thread.sleep(250);
		driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/a")).click();
		Thread.sleep(250);
		assertEquals(driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[5]")).getText(), "Fulfilled");
		
		/* LOGOUT + LOG IN AS OTHERCUS */
		Thread.sleep(250);
		driver.findElement(By.id("logout")).click();
		Thread.sleep(250);	
		driver.findElement(By.id("action")).click();
		driver.findElement(By.id("userName")).sendKeys("cusname");
		driver.findElement(By.id("password")).sendKeys("cuspass");
		driver.findElement(By.id("login")).click();
		
		/* CHECK ORDER STATUS - FULFILLED */
		Thread.sleep(250);
		driver.findElement(By.xpath("/html/bod1y/div[3]/div[1]/div[1]/a[2]")).click();
		Thread.sleep(250);
		assertEquals(driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[2]")).getText(), "Order confirmed");
		
		/* CHECK ORDER CANCEL */
		Thread.sleep(250);
		driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div[2]/div/table/tbody/tr/td[4]/a")).click();
		Thread.sleep(250);
		assertEquals(driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[2]/div[2]/div/table/tbody/tr/td")).getText(), "No data available in table");
	}

	@AfterEach
	public void tearDown() {
		driver.quit();
	}

}
