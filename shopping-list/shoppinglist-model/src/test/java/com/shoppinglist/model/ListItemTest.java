package com.shoppinglist.model;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.*;

import javax.validation.Validation;
import javax.validation.Validator;

import static org.hamcrest.CoreMatchers.*;
import static com.shoppinglist.commontests.listitem.ListItemForTestsRepository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.shoppinglist.model.ListItem;
import com.shoppinglist.repository.ShoppingListRepository;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import com.shoppinglist.exception.ItemAlreadyAddedException;
import com.shoppinglist.exception.ItemNotFoundException;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

public class ListItemTest {
	ListItem bread = listItemWithId(bread(), 1L);
	ListItem milk = listItemWithId(milk(), 2L);
	
	@Test
	public void getId() {
		assertThat(bread.getId(), is(equalTo(1L)));
	}
	
	@Test
	public void setItemName() {
		bread.setItemName("New Name");
		assertThat(bread.getItemName(), is(equalTo("New Name")));
	}
	
	@Test
	public void setQuantity() {
		bread.setQuantity(5);
		assertThat(bread.getQuantity(), is(equalTo(5)));
	}
	
	@Test
	public void setPurchased() {
		bread.setPurchased(true);
		assertThat(bread.isPurchased(), is(equalTo(true)));
	}
	
	@Test
	public void simpleEqualsContract() {
	    EqualsVerifier.simple()
	    	.forClass(ListItem.class)
	    	.suppress(Warning.SURROGATE_KEY)
	    	.verify();
	}
	
	@Test
	public void stringTests() {
		assertThat(bread.toString(), is(equalTo("ListItem [id=1, itemName=Half Slice Pan, quantity=2, purchased=false]")));
	}
}
