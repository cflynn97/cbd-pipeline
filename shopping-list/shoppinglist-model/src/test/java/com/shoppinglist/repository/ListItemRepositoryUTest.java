package com.shoppinglist.repository;

import static com.shoppinglist.commontests.listitem.ListItemForTestsRepository.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import com.shoppinglist.commontests.utils.DBCommandTransactionalExecutor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.shoppinglist.model.ListItem;

public class ListItemRepositoryUTest {
	private EntityManagerFactory emf;
	private EntityManager em;
	private ShoppingListRepository shoppingListRepository;
	private DBCommandTransactionalExecutor dBCommandTransactionalExecutor;

	@Before
	public void initTestCase() {
		emf = Persistence.createEntityManagerFactory("shoppinglistPU");
		em = emf.createEntityManager();
		shoppingListRepository = new ShoppingListRepository();
		shoppingListRepository.em = em;
		dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(em);
	}

	@After
	public void closeEntityManager() {
		em.close();
		emf.close();
	}

	@Test
	public void addListItemAndFindIt() {
		final Long listItemAddedId = dBCommandTransactionalExecutor.executeCommand(() -> {
			return shoppingListRepository.add(bread()).getId();
		});
		
		assertThat(listItemAddedId, is(notNullValue()));
		final ListItem listItem = shoppingListRepository.findById(listItemAddedId);
		assertThat(listItem, is(notNullValue()));
		assertThat(listItem.getItemName(), is(equalTo(bread().getItemName())));
	}
	
	@Test
	public void findShoppingList() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			shoppingList().forEach(shoppingListRepository::add);
			return null;
		});
		final List<ListItem> shoppingList = shoppingListRepository.findAll();
		assertThat(shoppingList.size(), is(equalTo(3)));
		assertThat(shoppingList.get(0).getItemName(), is(equalTo(bread().getItemName())));
	}

	@Test
	public void alreadyExistsForAdd() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			shoppingListRepository.add(bread());
			return null;
		});
		assertThat(shoppingListRepository.alreadyExists(bread()), is(equalTo(true)));
		assertThat(shoppingListRepository.alreadyExists(milk()), is(equalTo(false)));	
	}
	
	@Test
	public void alreadyExistsListItemWithId() {
		final ListItem listItem = dBCommandTransactionalExecutor.executeCommand(() -> {
			shoppingListRepository.add(milk());
			return shoppingListRepository.add(bread());
		});
		assertThat(shoppingListRepository.alreadyExists(listItem), is(equalTo(false)));

		listItem.setItemName(milk().getItemName());
		assertThat(shoppingListRepository.alreadyExists(listItem), is(equalTo(true)));

		listItem.setItemName(eggs().getItemName());
		assertThat(shoppingListRepository.alreadyExists(listItem), is(equalTo(false)));
	}
	
	@Test
	public void existsById() {
		final Long sneakerAddedId = dBCommandTransactionalExecutor.executeCommand(() -> {
			return shoppingListRepository.add(bread()).getId();
		});
		assertThat(shoppingListRepository.existsById(sneakerAddedId),is(equalTo(true)));
		assertThat(shoppingListRepository.existsById(999L),is(equalTo(false)));
	}
	
	@Test
	public void updateListItem() {
		final Long sneakerAddedId = dBCommandTransactionalExecutor.executeCommand(() -> {
			return shoppingListRepository.add(bread()).getId();
		});
		
		ListItem sneaker = shoppingListRepository.findById(sneakerAddedId);
		assertThat(sneaker.getItemName(), is(equalTo(bread().getItemName())));
		sneaker.setQuantity(4);
		
		dBCommandTransactionalExecutor.executeCommand(() -> {
			shoppingListRepository.update(sneaker);
			return null;
		});
		
		ListItem sneakerAfter = shoppingListRepository.findById(sneakerAddedId);
		assertThat(sneakerAfter.getQuantity(), is(equalTo(4)));
	}
}