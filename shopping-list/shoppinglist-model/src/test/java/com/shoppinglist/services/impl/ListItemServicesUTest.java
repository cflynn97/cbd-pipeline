package com.shoppinglist.services.impl;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.*;

import javax.validation.Validation;
import javax.validation.Validator;

import static org.hamcrest.CoreMatchers.*;
import static com.shoppinglist.commontests.listitem.ListItemForTestsRepository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.shoppinglist.model.ListItem;
import com.shoppinglist.repository.ShoppingListRepository;
import com.shoppinglist.exception.ItemAlreadyAddedException;
import com.shoppinglist.exception.ItemNotFoundException;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

public class ListItemServicesUTest {
	
	private ShoppingListServiceImpl shoppingListServices;
	private Validator validator;
	private ShoppingListRepository shoppingListRepository;

	
	@BeforeEach
	public void initTestCase() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
		shoppingListRepository = mock(ShoppingListRepository.class);
		shoppingListServices = new ShoppingListServiceImpl();
		((ShoppingListServiceImpl) shoppingListServices).validator = validator;
		((ShoppingListServiceImpl) shoppingListServices).shoppinglistRepository = shoppingListRepository;
	}

	@Test
	public void addItem() {
		when(shoppingListRepository.add(listItemWithId(bread(), 1L))).thenReturn(bread());
		ListItem li = shoppingListServices.add(listItemWithId(bread(), 1L));
		verify(shoppingListRepository).add(listItemWithId(bread(), 1L));
		assertThat(li.getItemName(), is(equalTo("Half Slice Pan")));
	}
	
	@Test
	public void addItemAlreadyExists() {
		doThrow(new ItemAlreadyAddedException()).when(shoppingListRepository).add(listItemWithId(bread(), 1L));
		@SuppressWarnings("unused")
		Throwable exception = assertThrows(ItemAlreadyAddedException.class, () -> {
			shoppingListServices.add(listItemWithId(bread(), 1L));
		});
	}
	
	@Test
	public void findShoppingList() {
		when(shoppingListRepository.findAll()).thenReturn(shoppingList());
		final List<ListItem> shoppingList = shoppingListServices.findAll();
		for(ListItem li : shoppingList) { System.out.println(li.getItemName());}
		assertThat(shoppingList.size(), is(equalTo(3)));
		assertThat(shoppingList.get(0).getItemName(), is(equalTo(bread().getItemName())));
		assertThat(shoppingList.get(1).getItemName(), is(equalTo(milk().getItemName())));
		assertThat(shoppingList.get(2).getItemName(), is(equalTo(eggs().getItemName())));
	}

	@Test
	public void findShoppingListEmpty() {
		when(shoppingListRepository.findAll()).thenReturn(new ArrayList<>());
		final List<ListItem> shoppingList = shoppingListServices.findAll();
		assertThat(shoppingList.isEmpty(), is(equalTo(true)));
	}

	@Test
	public void findListItemById() {
		when(shoppingListRepository.findById(1L)).thenReturn(listItemWithId(bread(), 1L));
		final ListItem listItem = shoppingListServices.findById(1L);
		assertThat(listItem, is(notNullValue()));
		assertThat(listItem.getId(), is(equalTo(1L)));
		assertThat(listItem.getItemName(), is(equalTo(bread().getItemName())));
	}

	@Test
	public void updateShoppingList() {
		when(shoppingListRepository.alreadyExists(listItemWithId(bread(), 1L))).thenReturn(false);
		when(shoppingListRepository.existsById(1L)).thenReturn(true);
		shoppingListServices.update(listItemWithId(bread(), 1L));
		verify(shoppingListRepository).update(listItemWithId(bread(), 1L));
	}
	
	@Test
	public void updateItemExists() {
		doThrow(new ItemNotFoundException()).when(shoppingListRepository).update(listItemWithId(bread(), 1L));
		@SuppressWarnings("unused")
		Throwable exception = assertThrows(ItemNotFoundException.class, () -> {
			shoppingListServices.update(listItemWithId(bread(), 1L));
		});
	}
	
	@Test
	public void findListItemByIdNotFound() {
		when(shoppingListRepository.findById(1L)).thenReturn(null);
		@SuppressWarnings("unused")
		Throwable exception = assertThrows(ItemNotFoundException.class, () -> {
			shoppingListServices.findById(1L);
		});
	}
	
	@Test
	public void deleteItem() {
		doNothing().when(shoppingListRepository).delete(1L);
		shoppingListServices.delete(1L);
		verify(shoppingListRepository).delete(1L);
	}
}
