create table shopping_list (
	id bigserial not null,
	itemName varchar(255) not null unique,
	quantity int not null,
    purchased boolean not null,
    PRIMARY KEY (id)
);