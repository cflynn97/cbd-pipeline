package com.shoppinglist.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.shoppinglist.model.ListItem;

@Stateless
public class ShoppingListRepository {
	
	@PersistenceContext
	EntityManager em;
	
	// create
	public ListItem add(final ListItem listItem) {
		em.persist(listItem);
		return listItem;
	}
	
	// read
	@SuppressWarnings("unchecked")
	public List<ListItem> findAll() {
		return em.createQuery("Select l From ListItem l ORDER BY l.purchased DESC").getResultList();
	}
	
	public ListItem findById(final Long id) {
		if (id==null) {
			return null;
		}
		return em.find(ListItem.class, id);
	}
	
	// update
	public boolean alreadyExists(final ListItem listItem) {
		final StringBuilder jpql = new StringBuilder();
		jpql.append("Select 1 From ListItem l where l.itemName = :itemName");
		if(listItem.getId()!=null) {
			jpql.append(" And l.id != :id");
		}
		final Query query = em.createQuery(jpql.toString());
		query.setParameter("itemName", listItem.getItemName());
		if (listItem.getId() != null) {
			query.setParameter("id",  listItem.getId());
		}
		return !query.setMaxResults(1).getResultList().isEmpty();
	}

	public boolean existsById(final Long id) {
		return !em.createQuery("Select 1 From ListItem l where l.id=:id")
				.setParameter("id",id)
				.setMaxResults(1)
				.getResultList()
				.isEmpty();
	}
	
	public ListItem update(ListItem listItem) {
		em.merge(listItem);
		return listItem;
	}
	
	// delete
	public void delete(Long id) {
		final ListItem listItem = em.find(ListItem.class, id);
		em.remove(listItem);
	}
}