package com.shoppinglist.services;

import java.util.List;

import javax.ejb.Local;

import com.shoppinglist.model.ListItem;
import com.shoppinglist.exception.ItemNotFoundException;

@Local
public interface ShoppingListServices {
	// create
	ListItem add(ListItem listItem);	
	// read
	List<ListItem> findAll();			
	ListItem findById(Long sneakerid);	
	// update
	ListItem update(ListItem sneaker) throws ItemNotFoundException;
	// delete
	void delete(Long id);
}
