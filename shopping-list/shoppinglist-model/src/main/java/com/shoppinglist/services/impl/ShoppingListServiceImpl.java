package com.shoppinglist.services.impl;

import javax.validation.Validator;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.shoppinglist.model.ListItem;
import com.shoppinglist.repository.ShoppingListRepository;
import com.shoppinglist.services.ShoppingListServices;
import com.shoppinglist.exception.ItemNotFoundException;

@Stateless
public class ShoppingListServiceImpl implements ShoppingListServices {
	
	@Inject
	Validator validator;
	
	@Inject
	ShoppingListRepository shoppinglistRepository;
	
	// create
	@Override
	public ListItem add(ListItem listItem) {
		return shoppinglistRepository.add(listItem);
	}
	
	// read
	@Override
	public List<ListItem> findAll() {
		return shoppinglistRepository.findAll();
	}

	@Override
	public ListItem findById(Long id) {
		final ListItem listItem = shoppinglistRepository.findById(id);
		if (listItem == null) {
			throw new ItemNotFoundException();
		}
		return listItem;
	}
	
	// update
	@Override
	public ListItem update(final ListItem listItem) {
		if(!shoppinglistRepository.existsById(listItem.getId())) {
			throw new ItemNotFoundException();
		}
		return shoppinglistRepository.update(listItem);
	}
	
	// delete
	@Override
	public void delete(final Long id) {
		shoppinglistRepository.delete(id);
	}
}
