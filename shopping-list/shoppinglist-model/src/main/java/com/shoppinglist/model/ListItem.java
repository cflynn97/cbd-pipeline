package com.shoppinglist.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "shopping_list")
public class ListItem implements Serializable {
	private static final long serialVersionUID = -5832648266346709584L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(unique=true)
	private String itemName;
	
	@NotNull
	private int quantity;
	private boolean purchased;

	public ListItem() {}

	public ListItem(String itemName, int quantity, boolean purchased) {
		this.itemName = itemName;
		this.quantity = quantity;
		this.purchased = purchased;
	}

	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}

	public String getItemName() {return itemName;}
	public void setItemName(String itemName) {this.itemName = itemName;}
	
	public int getQuantity() {return quantity;}
	public void setQuantity(int quantity) {this.quantity = quantity;}

	public boolean isPurchased() {return purchased;}
	public void setPurchased(boolean purchased) {this.purchased = purchased;}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListItem other = (ListItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ListItem [id=" + id + ", itemName=" + itemName + ", quantity=" + quantity + ", purchased=" + purchased + "]";
	}

}